# Demo Contacts

An example project to share some of my skills.

## Development server

Update your `proxy.json` to hit your back-end server.
Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Improvements

Let me know if you have some improvement to share, please.
