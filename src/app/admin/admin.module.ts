import { NgModule }         from "@angular/core";
import { FoundationModule } from "../@foundation/foundation.module";

import { AdminRoutingModule }          from "./admin-routing.module";
import { AdminComponent }              from "./admin.component";
import { ContentComponent }            from "./layout/content/content.component";
import { HeaderComponent }             from "./layout/header/header.component";
import { ContactsManipulateComponent } from "./views/contacts/contacts-manipulate/contacts-manipulate.component";
import { ContactsComponent }           from "./views/contacts/contacts.component";
import { GroupsManipulateComponent }   from "./views/groups/groups-manipulate/groups-manipulate.component";
import { GroupsComponent }             from "./views/groups/groups.component";

@NgModule({
    imports: [
        FoundationModule,
        AdminRoutingModule,
    ],
    declarations: [
        AdminComponent,
        HeaderComponent,
        ContentComponent,
        ContactsComponent,
        ContactsManipulateComponent,
        GroupsComponent,
        GroupsManipulateComponent,
    ],
})
export class AdminModule {
}
