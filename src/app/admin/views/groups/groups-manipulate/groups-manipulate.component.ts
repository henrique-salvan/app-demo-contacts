import { Component, OnInit }      from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { isNullOrUndefined }      from "util";
import { ManipulateComponent }    from "../../../../@foundation/components/views/manipulate/manipulate.component";
import { GroupInterface }         from "../../../../@foundation/interfaces/group.interface";
import { GroupService }           from "../../../../@foundation/services/group.service";

@Component({
    selector: "app-groups-manipulate",
    templateUrl: "./groups-manipulate.component.html",
    styleUrls: ["./groups-manipulate.component.scss"],
    providers: [
        GroupService,
    ],
})
export class GroupsManipulateComponent extends ManipulateComponent implements OnInit {

    public payload: GroupInterface;

    constructor(
        groupService: GroupService,
        activatedRoute: ActivatedRoute,
        private router: Router,
    ) {
        super(groupService, activatedRoute);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    onPayload() {
        super.onPayload();
        this.pageHeader.title = this.payload && !isNullOrUndefined(this.payload.id) ? `${this.payload.to_string_computed}` : "New Group";
    }

    public pageReturn() {
        this.router.navigate(["/admin/groups"]);
    }

    public getNewPayload(): GroupInterface {
        return { title: "" };
    }

    public valid(): boolean {
        return (
            !isNullOrUndefined(this.payload.title)
            && (this.payload.title.length >= 4)
        );
    }

}
