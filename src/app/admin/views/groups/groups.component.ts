import { Component, OnInit }   from "@angular/core";
import { Router }              from "@angular/router";
import { isNullOrUndefined }   from "util";
import { IndexComponent }      from "../../../@foundation/components/views/index/index.component";
import { PageHeaderInterface } from "../../../@foundation/components/views/pieces/page-header/page.header.interface";
import { FilterInterface }     from "../../../@foundation/interfaces/filter.interface";
import { GroupInterface }      from "../../../@foundation/interfaces/group.interface";
import { GroupService }        from "../../../@foundation/services/group.service";

@Component({
    selector: "app-groups",
    templateUrl: "./groups.component.html",
    styleUrls: ["./groups.component.scss"],
    providers: [
        GroupService,
    ],
})
export class GroupsComponent extends IndexComponent implements OnInit {

    public pageHeader: PageHeaderInterface = {
        title: "Groups",
    };

    //we can have a separated component for a big filter
    public filters: FilterInterface[] = [
        {
            column: "title",
            operator: "like",
            filter: "",
            manipulator: (filter) => {
                if (!isNullOrUndefined(filter) && filter.length) {
                    return `%${filter}%`;
                }
            },
        },
    ];

    constructor(
        groupService: GroupService,
        private router: Router,
    ) {
        super(groupService);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    manipulate(group?: GroupInterface) {
        if (isNullOrUndefined(group)) {
            this.router.navigate(["/admin/groups/store"]);
        } else {
            this.router.navigate(["/admin/groups", group.id, "edit"]);
        }
    }

}
