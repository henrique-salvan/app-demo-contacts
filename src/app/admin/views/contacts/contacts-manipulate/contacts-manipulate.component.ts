import { Component, OnInit }      from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { isNullOrUndefined }      from "util";
import { ManipulateComponent }    from "../../../../@foundation/components/views/manipulate/manipulate.component";
import { ContactInterface }       from "../../../../@foundation/interfaces/contact.interface";
import { GroupInterface }         from "../../../../@foundation/interfaces/group.interface";
import { ContactService }         from "../../../../@foundation/services/contact.service";
import { GroupService }           from "../../../../@foundation/services/group.service";

@Component({
    selector: "app-contacts-manipulate",
    templateUrl: "./contacts-manipulate.component.html",
    styleUrls: ["./contacts-manipulate.component.scss"],
    providers: [
        ContactService,
        GroupService,
    ],
})
export class ContactsManipulateComponent extends ManipulateComponent implements OnInit {

    public groups: GroupInterface[] = [];

    public payload: ContactInterface;

    constructor(
        contactService: ContactService,
        activatedRoute: ActivatedRoute,
        private groupService: GroupService,
        private router: Router,
    ) {
        super(contactService, activatedRoute);
    }

    ngOnInit() {
        super.ngOnInit();
        this.groupService.index().subscribe(response => {
            this.groups = response.content;
        });
    }

    onPayload() {
        super.onPayload();
        this.pageHeader.title = this.payload && !isNullOrUndefined(this.payload.id) ? `${this.payload.to_string_computed}` : "New Contact";
    }

    public pageReturn() {
        this.router.navigate(["/admin/contacts"]);
    }

    public getNewPayload(): ContactInterface {
        return { name: "" };
    }

    public valid(): boolean {
        return (
            !isNullOrUndefined(this.payload.name)
            && (this.payload.name.length >= 4)
        );
    }

}
