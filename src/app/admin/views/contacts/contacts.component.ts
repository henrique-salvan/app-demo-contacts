import { Component, OnInit }   from "@angular/core";
import { Router }              from "@angular/router";
import { isNullOrUndefined }   from "util";
import { IndexComponent }      from "../../../@foundation/components/views/index/index.component";
import { PageHeaderInterface } from "../../../@foundation/components/views/pieces/page-header/page.header.interface";
import { ContactInterface }    from "../../../@foundation/interfaces/contact.interface";
import { FilterInterface }     from "../../../@foundation/interfaces/filter.interface";
import { ContactService }      from "../../../@foundation/services/contact.service";

@Component({
    selector: "app-contacts",
    templateUrl: "./contacts.component.html",
    styleUrls: ["./contacts.component.scss"],
    providers: [
        ContactService,
    ],
})
export class ContactsComponent extends IndexComponent implements OnInit {

    public pageHeader: PageHeaderInterface = {
        title: "Contacts",
    };

    protected withs = [ContactService.withGroup];

    public items: ContactInterface[] = [];

    //we can have a separated component for a big filter
    public filters: FilterInterface[] = [
        {
            column: "name",
            operator: "like",
            filter: undefined,
            manipulator: (filter) => {
                if (!isNullOrUndefined(filter) && filter.length) {
                    return `%${filter}%`;
                }
            },
        },
    ];

    constructor(
        contactService: ContactService,
        private router: Router,
    ) {
        super(contactService);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    manipulate(contact?: ContactInterface) {

        if (isNullOrUndefined(contact)) {
            this.router.navigate(["admin/contacts/store"]);
        } else {
            this.router.navigate(["admin/contacts", contact.id, "edit"]);
        }

    }

}
