import { NgModule }                    from "@angular/core";
import { RouterModule, Routes }        from "@angular/router";
import { AdminComponent }              from "./admin.component";
import { ContactsManipulateComponent } from "./views/contacts/contacts-manipulate/contacts-manipulate.component";
import { ContactsComponent }           from "./views/contacts/contacts.component";
import { GroupsManipulateComponent }   from "./views/groups/groups-manipulate/groups-manipulate.component";
import { GroupsComponent }             from "./views/groups/groups.component";

const routes: Routes = [
    {
        path: "",
        component: AdminComponent,
        children: [
            { path: "", redirectTo: "contacts", pathMatch: "full" },
            { path: "contacts", component: ContactsComponent },
            { path: "contacts/store", component: ContactsManipulateComponent },
            { path: "contacts/:id/edit", component: ContactsManipulateComponent },
            { path: "groups", component: GroupsComponent },
            { path: "groups/store", component: GroupsManipulateComponent },
            { path: "groups/:id/edit", component: GroupsManipulateComponent },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AdminRoutingModule {
}
