import { Component, OnInit } from "@angular/core";
import { Router }            from "@angular/router";
import { AuthStorage }       from "../../../@foundation/helpers/storage";

@Component({
    selector: "app-header",
    templateUrl: "./header.component.html",
    styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {

    constructor(
        private router: Router,
    ) {
    }

    ngOnInit() {
    }

    logout() {
        AuthStorage.remove(AuthStorage.user);
        this.router.navigate(["/auth"]);
    }

}
