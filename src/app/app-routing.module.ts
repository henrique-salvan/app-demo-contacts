import { NgModule }             from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { Guard }                from "./@foundation/helpers/guard";

const routes: Routes = [
    {
        path: "auth",
        loadChildren: "app/auth/auth.module#AuthModule",
    },
    {
        path: "admin",
        loadChildren: "app/admin/admin.module#AdminModule",
        canActivate: [Guard],
    },
    {
        path: "",
        redirectTo: "auth",
        pathMatch: "full",
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
