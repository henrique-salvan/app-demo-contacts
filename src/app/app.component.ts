import { Component } from "@angular/core";

@Component({
    selector: "app-root",
    template: `

        <!-- Confirmation component -->
        <app-confirmation></app-confirmation>
        
        <!-- Toast component -->
        <app-toast></app-toast>

        <!-- Waiting component -->
        <app-waiting></app-waiting>
        
        <!-- Router -->
        <router-outlet></router-outlet>

    `,
})
export class AppComponent {
    title = "app";
}
