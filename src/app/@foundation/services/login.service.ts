import { Observable }        from "rxjs/Observable";
import { LoginInterface }    from "../interfaces/login.interface";
import { ResponseInterface } from "../interfaces/response.interface";
import { UserInterface }     from "../interfaces/user.interface";
import { Service }           from "./service";

export class LoginService extends Service<UserInterface> {

    auth() {
        return false;
    }

    resource() {
        return "login";
    }

    login(login: LoginInterface): Observable<ResponseInterface<UserInterface>> {
        return this.httpClient.post<ResponseInterface<UserInterface>>(`${this.uri}`, login);
    }

    user(): Observable<ResponseInterface<UserInterface>> {
        return this.httpClient.get<ResponseInterface<UserInterface>>(`${this.uri}/user`);
    }

}
