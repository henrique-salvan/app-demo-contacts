import { ContactInterface } from "../interfaces/contact.interface";
import { Service }          from "./service";

export class ContactService extends Service<ContactInterface> {

    public static withGroup: string = "group";

    auth() {
        return true;
    }

    resource() {
        return "contacts";
    }

}
