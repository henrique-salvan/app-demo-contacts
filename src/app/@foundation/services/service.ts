import { HttpClient, HttpHeaders }                 from "@angular/common/http";
import { Injectable }                              from "@angular/core";
import { Observable }                              from "rxjs/Observable";
import { isFunction, isNullOrUndefined, isNumber } from "util";
import { AuthStorage }                             from "../helpers/storage";
import { FilterInterface }                         from "../interfaces/filter.interface";
import { ResponseInterface }                       from "../interfaces/response.interface";
import { UserInterface }                           from "../interfaces/user.interface";
import { settings }                                from "../settings";

@Injectable()
export abstract class Service<T> {

    protected readonly uri: string;

    //constructor has to be public, for mitigate providers problem
    public constructor(
        protected httpClient: HttpClient,
    ) {
        this.uri = `${settings.getPrefixApi()}/${this.resource()}`;
    }

    abstract resource(): string;

    abstract auth(): boolean;

    index(
        page?: number,
        perPage?: number,
        withs?: string[],
        filters?: FilterInterface[],
    ): Observable<ResponseInterface<T[]>> {
        return this.httpClient.get<ResponseInterface<T[]>>(`${this.uri}${this.joinParameters([
            this.getParameterPage(page),
            this.getParameterPerPage(perPage),
            this.getParameterWiths(withs),
            this.getParameterFilter(filters),
        ])}`, { headers: this.getHeaders() });
    }

    show(id, withs?: string[]): Observable<ResponseInterface<T>> {
        return this.httpClient.get<ResponseInterface<T>>(`${this.uri}/${id}${this.joinParameters([
            this.getParameterWiths(withs),
        ])}`, { headers: this.getHeaders() });
    }

    create(T): Observable<ResponseInterface<T>> {
        return this.httpClient.post<ResponseInterface<T>>(`${this.uri}`, T, { headers: this.getHeaders() });
    }

    update(T): Observable<ResponseInterface<T>> {
        return this.httpClient.put<ResponseInterface<T>>(`${this.uri}/${T.id}`, T, { headers: this.getHeaders() });
    }

    destroy(T): Observable<ResponseInterface<T>> {
        return this.httpClient.delete<ResponseInterface<T>>(`${this.uri}/${T.id}`, { headers: this.getHeaders() });
    }

    protected joinParameters(parameters: string[]): string {
        return `?${parameters.filter(f => {
            return !isNullOrUndefined(f) && f.length;
        }).join("&")}`;
    }

    protected getParameterPerPage(perPage: number): string {
        perPage = isNumber(perPage) ? perPage : 100;
        return `per-page=${perPage}`;
    }

    protected getParameterPage(page: number): string {
        page = isNumber(page) ? page : 1;
        return `page=${page}`;
    }

    protected getParameterWiths(withs?: string[]): string {
        if (!isNullOrUndefined(withs) && withs.length >= 1) {
            return `withs=${withs.join(",")}`;
        }
        return "";
    }

    protected getParameterFilter(filters?: FilterInterface[]): string {

        if (filters && filters.length) {

            return filters.filter((filter) => {

                let manipulated = filter.filter;

                if (!isNullOrUndefined(filter.manipulator) && isFunction(filter.manipulator)) {
                    manipulated = filter.manipulator(manipulated);
                }

                if (!isNullOrUndefined(manipulated)) {
                    filter.manipulated = encodeURIComponent(manipulated);
                } else {
                    filter.manipulated = undefined;
                }

                return !isNullOrUndefined(filter.manipulated);

            }).map((filter, i) => {
                return `filters[${i}][0]=${filter.column}&filters[${i}][1]=${filter.operator}&filters[${i}][2]=${filter.manipulated}`;
            }).join("&");

        }

        return "";
    }

    protected getHeaders(): HttpHeaders {
        let httpHeaders = new HttpHeaders();

        if (this.auth()) {
            let user = <UserInterface>AuthStorage.get(AuthStorage.user);
            httpHeaders = httpHeaders.append("demo-contacts-token", user.api_token);
        }

        return httpHeaders;
    }

}
