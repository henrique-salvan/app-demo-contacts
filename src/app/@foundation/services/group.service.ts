import { GroupInterface } from "../interfaces/group.interface";
import { Service }        from "./service";

export class GroupService extends Service<GroupInterface> {

    auth() {
        return true;
    }

    resource() {
        return "groups";
    }

}
