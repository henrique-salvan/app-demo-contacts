import { CommonModule }                  from "@angular/common";
import { HttpClientModule }              from "@angular/common/http";
import { NgModule }                      from "@angular/core";
import { FormsModule }                   from "@angular/forms";
import { ClarityModule }                 from "@clr/angular";

import "./app.icons";
import { ConfirmationComponent }         from "./components/confirmation/confirmation.component";
import { ToastComponent }                from "./components/toast/toast.component";
import { DataGridLoadingItemsComponent } from "./components/views/pieces/data-grid-loading-items/data-grid-loading-items.component";
import { ErrorsFormComponent }           from "./components/views/pieces/errors-form/errors-form.component";
import { PageFilterComponent }           from "./components/views/pieces/page-filter/page-filter.component";
import { PageHeaderComponent }           from "./components/views/pieces/page-header/page-header.component";
import { RowFormComponent }              from "./components/views/pieces/row-form/row-form.component";
import { WaitingComponent }              from "./components/waiting/waiting.component";
import { Guard }                         from "./helpers/guard";
import { LoginService }                  from "./services/login.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ClarityModule,
        HttpClientModule,
    ],
    declarations: [
        ToastComponent,
        WaitingComponent,
        PageHeaderComponent,
        PageFilterComponent,
        DataGridLoadingItemsComponent,
        RowFormComponent,
        ErrorsFormComponent,
        ConfirmationComponent,
    ],
    exports: [
        CommonModule,
        FormsModule,
        ClarityModule,

        ToastComponent,
        WaitingComponent,
        PageHeaderComponent,
        PageFilterComponent,
        DataGridLoadingItemsComponent,
        RowFormComponent,
        ErrorsFormComponent,
        ConfirmationComponent,
    ],
    providers: [
        LoginService,
        Guard,
    ],
})
export class FoundationModule {
}
