import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription }                 from "rxjs/Subscription";
import { isNullOrUndefined }            from "util";
import { ToastInterface }               from "../../interfaces/toast.interface";
import { Toast }                        from "./toast";

@Component({
    selector: "app-toast",
    templateUrl: "./toast.component.html",
    styleUrls: ["./toast.component.scss"],
})
export class ToastComponent implements OnInit, OnDestroy {

    public toastInterface: ToastInterface;
    private subscription: Subscription;
    private timeOut: any = undefined;

    constructor() {
    }

    ngOnInit() {
        this.subscription = Toast.toastState.subscribe((state: ToastInterface) => {

            //change state|toastInterface
            this.toastInterface = state;

            //reset timeout
            if (!isNullOrUndefined(this.timeOut)) {
                clearTimeout(this.timeOut);
            }

            //create new timeout
            this.timeOut = setTimeout(() => {
                this.toastInterface.show = false;
            }, this.toastInterface.time);

        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
