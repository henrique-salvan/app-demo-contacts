import { Injectable }        from "@angular/core";
import { Subject }           from "rxjs/Subject";
import { isNullOrUndefined } from "util";
import { ToastInterface }    from "../../interfaces/toast.interface";

@Injectable()
export class Toast {

    private static defaultDelay: number = 5000;

    public static typeNormal: string = "normal";
    public static typeSuccess: string = "success";
    public static typeWarning: string = "warning";
    public static typeDanger: string = "danger";

    private static toastSubject = new Subject<ToastInterface>();
    public static toastState = Toast.toastSubject.asObservable();

    static show(message: string, type?: string, time?: number) {
        Toast.toastSubject.next(<ToastInterface>{
            show: true,
            message: this.getMessage(message, null),
            type: isNullOrUndefined(type) ? Toast.typeNormal : type,
            time: isNullOrUndefined(time) ? Toast.defaultDelay : time,
        });
    }

    static normal(message: string) {
        this.show(this.getMessage(message, null), Toast.typeNormal);
    }

    static success(message?: string) {
        this.show(this.getMessage(message, "Action performed successfully."), Toast.typeSuccess);
    }

    static warning(message: string) {
        this.show(this.getMessage(message, null), Toast.typeWarning);
    }

    static danger(message?: string) {
        this.show(this.getMessage(message, "Something went wrong. Try again later, please."), Toast.typeDanger);
    }

    private static getMessage(message: string, defaultMessage: string): string {
        if (isNullOrUndefined(message) || message.length === 0) {
            if (isNullOrUndefined(defaultMessage) || defaultMessage.length === 0) {
                throw "Toast don't have a message to show";
            }
            return defaultMessage;
        }
        return message;
    }

}
