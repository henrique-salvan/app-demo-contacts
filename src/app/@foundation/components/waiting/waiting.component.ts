import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription }                 from "rxjs/Subscription";
import { WaitingInterface }             from "../../interfaces/waiting.interface";
import { Waiting }                      from "./waiting";

@Component({
    selector: "app-waiting",
    templateUrl: "./waiting.component.html",
    styleUrls: ["./waiting.component.scss"],
})
export class WaitingComponent implements OnInit, OnDestroy {

    private count: number = 0;

    public show: boolean = false;
    private subscription: Subscription;

    constructor() {
    }

    ngOnInit() {
        this.subscription = Waiting.waitingState.subscribe((state: WaitingInterface) => {
            if (state.show) {
                this.count++;
            } else {
                this.count--;
            }
            this.countCheck();
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    private countCheck() {
        if (this.count >= 1) {
            this.show = true;
        } else {
            this.count = 0;
            this.show = false;
        }
    }

}
