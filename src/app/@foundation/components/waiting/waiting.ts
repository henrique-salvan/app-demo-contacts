import { Injectable }        from "@angular/core";
import { Subject }           from "rxjs/Subject";
import { isNullOrUndefined } from "util";
import { WaitingInterface }  from "../../interfaces/waiting.interface";

@Injectable()
export class Waiting {

    private static waitingSubject = new Subject<WaitingInterface>();
    public static waitingState = Waiting.waitingSubject.asObservable();

    constructor() {
    }

    static show() {
        Waiting.waitingSubject.next(<WaitingInterface>{ show: true });
    }

    static hide() {
        Waiting.waitingSubject.next(<WaitingInterface>{ show: false });
    }

    static hideDelay(delay?: number) {
        setTimeout(() => {
            Waiting.waitingSubject.next(<WaitingInterface>{ show: false });
        }, isNullOrUndefined(delay) ? 1000 : delay);
    }

}
