import { Component, EventEmitter, OnInit, Output } from "@angular/core";

@Component({
    selector: "[app-page-filter]",
    templateUrl: "./page-filter.component.html",
    styleUrls: ["./page-filter.component.scss"],
})
export class PageFilterComponent implements OnInit {

    @Output()
    onSearch: EventEmitter<any> = new EventEmitter();


    constructor() {
    }

    ngOnInit() {
    }

    search($event: Event) {
        $event.preventDefault();
        this.onSearch.emit();
    }

}
