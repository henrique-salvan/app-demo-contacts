import { Component, Input, OnInit } from "@angular/core";

@Component({
    selector: "app-row-form",
    templateUrl: "./row-form.component.html",
    styleUrls: ["./row-form.component.scss"],
})
export class RowFormComponent implements OnInit {

    @Input()
    public label: string;

    constructor() {
    }

    ngOnInit() {
    }

}
