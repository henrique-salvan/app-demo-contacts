import { Component, Input, OnInit } from "@angular/core";
import { ErrorInterface }           from "../../../../interfaces/error.interface";

@Component({
    selector: "app-errors-form",
    templateUrl: "./errors-form.component.html",
    styleUrls: ["./errors-form.component.scss"],
})
export class ErrorsFormComponent implements OnInit {

    @Input()
    public errors: ErrorInterface[];

    constructor() {
    }

    ngOnInit() {
    }

}
