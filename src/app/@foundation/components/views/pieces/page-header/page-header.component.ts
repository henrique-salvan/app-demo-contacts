import { Component, Input, OnInit } from "@angular/core";
import { PageHeaderInterface }      from "./page.header.interface";

@Component({
    selector: "app-page-header",
    templateUrl: "./page-header.component.html",
    styleUrls: ["./page-header.component.scss"],
})
export class PageHeaderComponent implements OnInit {

    @Input()
    public pageHeader: PageHeaderInterface;

    constructor() {
    }

    ngOnInit() {
    }

}
