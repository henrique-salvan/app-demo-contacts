import { OnInit }                      from "@angular/core";
import { ActivatedRoute }              from "@angular/router";
import { isNullOrUndefined, isNumber } from "util";
import { ErrorInterface }              from "../../../interfaces/error.interface";
import { ModelInterface }              from "../../../interfaces/model.interface";
import { ResponseInterface }           from "../../../interfaces/response.interface";
import { Service }                     from "../../../services/service";
import { Toast }                       from "../../toast/toast";
import { Waiting }                     from "../../waiting/waiting";
import { PageHeaderInterface }         from "../pieces/page-header/page.header.interface";

export abstract class ManipulateComponent implements OnInit {

    public pageHeader: PageHeaderInterface = { title: "" };

    public errors: ErrorInterface[] = [];

    protected showWiths = [];

    abstract payload: ModelInterface;

    abstract getNewPayload(): ModelInterface;

    abstract valid(): boolean;

    abstract pageReturn(): void;

    protected constructor(
        protected service: Service<ModelInterface>,
        protected activatedRoute: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(data => {
            if (!isNullOrUndefined(data.id)) {
                Waiting.show();
                this.service.show(data.id, this.showWiths).subscribe(data => {
                    if (data.success) {
                        this.payload = data.content;
                    }
                    Waiting.hide();
                    this.onPayload();
                });
            } else {
                this.onPayload();
            }
        });
    }

    protected onPayload() {
        if (isNullOrUndefined(this.payload) || isNullOrUndefined(this.payload.id)) {
            this.payload = this.getNewPayload();
        }
    }

    contentButton(): string {
        if (!isNullOrUndefined(this.payload)) {
            return isNullOrUndefined(this.payload.id) ? "Save" : "Edit";
        }
        return "Loading";
    }

    submit() {
        if (this.valid()) {
            Waiting.show();
            if (!isNullOrUndefined(this.payload.id) && isNumber(this.payload.id)) {
                this.service.update(this.payload).subscribe(data => this.subscribeNext(data));
            } else {
                this.service.create(this.payload).subscribe(data => this.subscribeNext(data));
            }
        }
    }

    protected subscribeNext(data: ResponseInterface<ModelInterface>) {
        Waiting.hide();
        if (data.success) {
            this.pageReturn();
            Toast.success();
        } else {
            this.errors = <ErrorInterface[]>data.content["errors"];
            if (!this.errors.length) {
                Toast.danger();
            }
        }
    }

}
