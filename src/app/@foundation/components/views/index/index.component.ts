import { HostListener, OnInit }                         from "@angular/core";
import { __nativeWindow, __pageHeight, __screenHeight } from "../../../helpers/functions";
import { FilterInterface }                              from "../../../interfaces/filter.interface";
import { ModelInterface }                               from "../../../interfaces/model.interface";
import { Service }                                      from "../../../services/service";
import { settings }                                     from "../../../settings";
import { Confirmation }                                 from "../../confirmation/confirmation";
import { Toast }                                        from "../../toast/toast";
import { Waiting }                                      from "../../waiting/waiting";
import { PageHeaderInterface }                          from "../pieces/page-header/page.header.interface";

export abstract class IndexComponent implements OnInit {

    abstract pageHeader: PageHeaderInterface;

    public items: ModelInterface[] = [];

    public loadingItems: boolean = false;

    public pagination: {
        page: number
        perPage: number
        hasMoreItems: boolean
    };

    protected filters: FilterInterface[] = [];

    protected withs = [];

    protected constructor(
        protected service: Service<ModelInterface>,
    ) {
    }

    ngOnInit() {
        this.resetPagination();
        this.scrollPagination();
    }

    private resetPagination() {
        this.items = [];
        this.pagination = {
            page: 1,
            perPage: settings.paginationPerPageDefault,
            hasMoreItems: true,
        };
    }

    private feedPagination(items: ModelInterface[]) {

        this.pagination.page++;

        if (this.pagination.perPage > items.length) {
            this.pagination.hasMoreItems = false;
        }

        if (items.length) {
            this.onLoadItems(items);
            this.items.push(...items);
        } else {
            console.log("called ajax with empty values");
        }

    }

    protected onLoadItems(items: ModelInterface[]) {
    };

    @HostListener("window:scroll", [])
    onWindowScroll() {
        if (this.pagination.hasMoreItems && !this.loadingItems) {
            if ((__nativeWindow().scrollY + __screenHeight() + 200) >= __pageHeight()) {
                this.scrollPagination(true);
            }
        }
    }

    public scrollPagination(isScroll: boolean = false, search: boolean = false) {

        this.loadingItems = true;

        this.service.index(
            this.pagination.page,
            this.pagination.perPage,
            this.withs,
            this.filters,
        ).subscribe(data => {

            if (search) {
                this.resetPagination();
            }

            this.feedPagination(data.content);
            this.loadingItems = false;

        });

    }

    public search() {
        this.resetPagination();
        this.scrollPagination(false, true);
    }

    public emptyItems(): boolean {
        return (!this.loadingItems && (!this.items && this.items.length === 0));
    }

    public destroy(item: ModelInterface) {
        Confirmation.danger(`Are you sure you want to remove "${item.to_string_computed}"?`).subscribe(confirm => {
            if (confirm) {
                Waiting.show();
                this.service.destroy(item).subscribe(data => {
                    Waiting.hide();
                    if (data.success) {
                        Toast.success();
                        this.ngOnInit();
                    } else {
                        Toast.danger();
                    }
                });
            }
        });
    }

}
