import { Injectable }                                         from "@angular/core";
import { Subject }                                            from "rxjs/Subject";
import { ConfirmationButtonInterface, ConfirmationInterface } from "../../interfaces/confirmation.interface";

@Injectable()
export class Confirmation {

    private static confirmationSubject = new Subject<ConfirmationInterface>();
    public static confirmationState = Confirmation.confirmationSubject.asObservable();

    private static classNormal = "btn-primary";
    private static classSuccess = "btn-success";
    private static classWarning = "btn-warning";
    private static classDanger = "btn-danger";

    constructor() {
    }

    /**
     * @param {string} question
     * @param button
     * @returns {Subject<boolean>}
     */
    private static confirm(question: string, button: ConfirmationButtonInterface) {

        let subject = new Subject<boolean>();

        Confirmation.confirmationSubject.next(<ConfirmationInterface>{
            question: question,
            button: button,
            subject: subject,
        });

        return subject;

    }

    public static normal(question: string, buttonContent?: string) {
        return this.confirm(question, { content: buttonContent, class: this.classNormal });
    }

    public static success(question: string, buttonContent?: string) {
        return this.confirm(question, { content: buttonContent, class: this.classSuccess });
    }

    public static warning(question: string, buttonContent?: string) {
        return this.confirm(question, { content: buttonContent, class: this.classWarning });
    }

    public static danger(question: string, buttonContent?: string) {
        return this.confirm(question, { content: buttonContent, class: this.classDanger });
    }

}
