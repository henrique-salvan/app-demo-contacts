import { Component, OnInit }     from "@angular/core";
import { Subscription }          from "rxjs/Subscription";
import { isNullOrUndefined }     from "util";
import { ConfirmationInterface } from "../../interfaces/confirmation.interface";
import { Confirmation }          from "./confirmation";

@Component({
    selector: "app-confirmation",
    templateUrl: "./confirmation.component.html",
    styleUrls: ["./confirmation.component.scss"],
})
export class ConfirmationComponent implements OnInit {

    public opened: boolean = false;

    public confirmationInterface: ConfirmationInterface;

    private subscription: Subscription;

    constructor() {
    }

    ngOnInit() {
        this.subscription = Confirmation.confirmationState.subscribe((confirmation: ConfirmationInterface) => {

            this.confirmationInterface = confirmation;

            if (!(
                !isNullOrUndefined(this.confirmationInterface.button.content)
                && this.confirmationInterface.button.content.length >= 1
            )) {
                this.confirmationInterface.button.content = "Yes";
            }

            this.opened = true;

        });
    }

    public exec(choice: boolean) {
        this.confirmationInterface.subject.next(choice);
        this.confirmationInterface = undefined;
        this.opened = false;
    }

}
