import { ClarityIcons }                  from "@clr/icons";
import { ClrShapePencil, ClrShapeTrash } from "@clr/icons/shapes/essential-shapes";

ClarityIcons.add({
    pencil: ClrShapePencil,
    trash: ClrShapeTrash,
});
