import { ModelInterface } from "./model.interface";

export interface UserInterface extends ModelInterface {
    name?: string
    email?: string
    password?: string
    api_token?: string
}
