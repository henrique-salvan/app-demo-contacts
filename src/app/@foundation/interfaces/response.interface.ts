export interface ResponseInterface<T> {
    content?: T
    success: boolean
}
