import { Subject } from "rxjs/Subject";

export interface ConfirmationInterface {
    question: string
    button: ConfirmationButtonInterface
    subject: Subject<boolean>
}

export interface ConfirmationButtonInterface {
    content?: string
    class: string
}
