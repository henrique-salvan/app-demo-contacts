import { GroupInterface } from "./group.interface";
import { ModelInterface } from "./model.interface";

export interface ContactInterface extends ModelInterface {
    name: string
    email?: string
    cellphone?: string
    group_id?: number

    group?: GroupInterface
}
