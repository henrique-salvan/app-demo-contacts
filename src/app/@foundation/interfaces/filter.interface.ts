export interface FilterInterface {
    column: string
    operator: string
    filter: any
    manipulator?: any
    manipulated?: any
}
