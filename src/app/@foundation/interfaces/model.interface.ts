export interface ModelInterface {
    id?: number;
    created_at?: string;
    updated_at?: string;
    to_string_computed?: string;
}
