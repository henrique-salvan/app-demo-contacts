export interface ToastInterface {
    show: boolean
    time?: number
    message?: string
    type?: string
}
