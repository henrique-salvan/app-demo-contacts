import { ModelInterface } from "./model.interface";

export interface GroupInterface extends ModelInterface {
    title: string
}
