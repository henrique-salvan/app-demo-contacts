export let settings = {
    storageKey: "demo-contacts",
    paginationPerPageDefault: 50,
    getPrefixApi: (): string => {
        return "/api";
    },
};
