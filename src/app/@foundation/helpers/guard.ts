import { Injectable }                                                       from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { isNullOrUndefined }                                                from "util";
import { UserInterface }                                                    from "../interfaces/user.interface";
import { AuthStorage }                                                      from "./storage";

@Injectable()
export class Guard implements CanActivate {

    constructor(
        private router: Router,
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

        let user: UserInterface = AuthStorage.get(AuthStorage.user);

        if ((isNullOrUndefined(user) || isNullOrUndefined(user.api_token))) {
            this.router.navigate(["/auth"]);
        } else {
            return true;
        }

    }

}
