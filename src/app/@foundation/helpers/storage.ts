import { Injectable }        from "@angular/core";
import { isNullOrUndefined } from "util";
import { settings }          from "../settings";
import { __nativeWindow }    from "./functions";

abstract class AbstractStorage {

    private static appKey = "__" + settings.storageKey + "__";

    private static getKey(key: string): string {
        return (AbstractStorage.appKey + key);
    }

    private static getStorage(wildCard: string) {
        return __nativeWindow()[wildCard + "Storage"];
    }

    protected static set(wildCard: string, key: string, content: Object): void {
        if (!isNullOrUndefined(content)) {
            AbstractStorage.getStorage(wildCard).setItem(
                AbstractStorage.getKey(key), JSON.stringify({ content: content }),
            );
        }
    }

    protected static get(wildCard: string, key: string): any {
        let content = AbstractStorage.getStorage(wildCard).getItem(AbstractStorage.getKey(key));
        if (!isNullOrUndefined(content)) {
            return JSON.parse(content).content;
        }
        return null;
    }

    protected static remove(wildCard: string, key: string): void {
        AbstractStorage.getStorage(wildCard).removeItem(AbstractStorage.getKey(key));
    }

}

@Injectable()
export class LocalStorage extends AbstractStorage {

    private static wildCard = "local";

    public static set(key: string, content: Object): void {
        super.set(LocalStorage.wildCard, key, content);
    }

    public static get(key: string): any {
        return super.get(LocalStorage.wildCard, key);
    }

    public static remove(key: string): void {
        super.remove(LocalStorage.wildCard, key);
    }

}

@Injectable()
export class SessionStorage extends AbstractStorage {

    private static wildCard = "session";

    public static set(key: string, content: Object): void {
        super.set(SessionStorage.wildCard, key, content);
    }

    public static get(key: string): any {
        return super.get(SessionStorage.wildCard, key);
    }

    public static remove(key: string): void {
        super.remove(SessionStorage.wildCard, key);
    }

}

@Injectable()
export class AuthStorage extends SessionStorage {
    public static user = "authenticated-user";
}
