export function __nativeWindow(): any {
    return window;
}

export function __nativeDocument(): any {
    return document;
}

export function __screenHeight(): number {
    return Math.max(__nativeDocument().documentElement.clientHeight, __nativeWindow().innerHeight);
}

export function __pageHeight(): number {
    return __nativeDocument().body.scrollHeight;
}
