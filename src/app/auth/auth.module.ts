import { NgModule }          from "@angular/core";
import { FoundationModule }  from "../@foundation/foundation.module";
import { AuthRoutingModule } from "./auth-routing.module";
import { AuthComponent }     from "./auth.component";
import { LoginComponent }    from "./login/login.component";

@NgModule({
    imports: [
        FoundationModule,
        AuthRoutingModule,
    ],
    declarations: [
        AuthComponent,
        LoginComponent,
    ],
})
export class AuthModule {
}
