import { Component, OnInit } from "@angular/core";
import { Router }            from "@angular/router";
import { Toast }             from "../../@foundation/components/toast/toast";
import { Waiting }           from "../../@foundation/components/waiting/waiting";
import { AuthStorage }       from "../../@foundation/helpers/storage";
import { LoginInterface }    from "../../@foundation/interfaces/login.interface";
import { LoginService }      from "../../@foundation/services/login.service";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
    providers: [
        LoginService,
    ],
})
export class LoginComponent implements OnInit {

    public login: LoginInterface = {
        email: "demo@demo.com",
        password: "secret",
    };

    constructor(private loginService: LoginService, private router: Router) {
    }

    ngOnInit() {
    }

    submit() {
        if (this.validate()) {
            Waiting.show();
            this.loginService.login(this.login).subscribe(data => {
                if (data.success) {
                    AuthStorage.set(AuthStorage.user, data.content);
                    this.router.navigate(["/admin"]);
                } else {
                    Toast.danger("Password or email invalid");
                }
                Waiting.hideDelay();
            });
        } else {
            Toast.warning("Complete all fields");
        }
    }

    validate() {
        return (
            this.login.email
            && this.login.email.length >= 4
            && this.login.password
            && this.login.password.length >= 4
        );
    }

}
